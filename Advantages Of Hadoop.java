/*
Advantages of Hadoop
	Hadoop framework allows the user to quickly write and test distributed systems. It is efficient, and it automatic distributes the data and work across the machines and in turn, utilizes the underlying parallelism of the CPU cores.

	 Hadoop does not rely on hardware to provide fault-tolerance and high availability (FTHA), rather Hadoop library itself has been designed to detect and handle failures at the application layer.
	 
	 Servers can be added or removed from the cluster dynamically and Hadoop continues to operate without interruption.
	 
	 Another big advantage of Hadoop is that apart from being open source, it is compatible on all the platforms since it is Java based.

*/