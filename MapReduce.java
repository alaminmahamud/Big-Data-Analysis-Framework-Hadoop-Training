/*
MapReduce

MapReduce is a parallel programming model for writing 
distributed applications devised at Google for 
efficient processing of large amounts of data (multi-
terabyte data-sets), on large clusters (thousands of 
nodes) of commodity hardware in a reliable, fault-
tolerant manner. The MapReduce program runs on Hadoop 
which is an Apache open-source framework


	
*/