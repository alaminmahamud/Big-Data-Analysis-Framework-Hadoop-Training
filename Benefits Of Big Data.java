
/*

	# Benefits of Big Data
	
		Using the information kept in the social network like Facebook, 
		the marketing agencies are learning about the response for their
		campaigns, promotions, and other advertising mediums. 
		
		Using the information in the social media like preferences and 
		product perception of their consumers, product companies and 
		retail organizations are planning their production.

		Using the data regarding the previous medical history of patients,
		hospitals are providing better and quick service.		

	# Big Data Technologies

		To harness the power of big data, you would require an infrastructure
		that can manage and process huge volumes of structured and unstructured 
		data in real-time and can protect data privacy and security.

	



*/
