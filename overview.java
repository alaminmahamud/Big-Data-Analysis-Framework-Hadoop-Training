/*
	
	#1. Big Data OverView

	90% of the world's data was generated in the last few years

	beginning of time ~ 2003
		5 billion gigabytes
		entire football field if we try to keep them in a discs

	2011
		in every two days same amount data was created 

	2013 
		in every ten minutes

	# What is Big data?

	Big data is a collection of dataset than cannot be processed using traditional computing techniques. It is not a single technique or a tool, rather it involves many areas of business and technology

	# What comes under Big Data? 

		* Black box Data
			It is a component of helicopter, airplanes and jets etc. 
			It Capture voices of the flight crew, recordings of microphones and earphones, the performance information of the aircraft

		* Social Media Data
			Social Media such as Facebook and Twitter hold information and the views posted by millions of people across the globe

		* Stock Exchange Data
			The Stock Exchange Data holds the Information about buy and sell  decissions made on a share of different companies made by the customers

		* Power Grid Data
			THe Power grid data holds the information consumed by a particular node with respect to a base station

		* Transport Data
			Transport Data includes model, capacity, distance and availability of a vehicle

		* Search Engine Data
			Search Engine Retrieves Lots of Data from Different Databases
	
	Thus big data includes huge volumen, high velocity and extensible variety if data
	the data will be of three different types

		* Structured Data - RDBMS
		* Semi Structured Data - XML Data
		* UnStructured Data - Word, PDF, Text, Media Logs
	 

*/