/*

# Anlytical Big Data

These includes systems like Massively Parallel 
Processing (MPP) database systems and MapReduce that 
provide analytical capabilities for retrospective and 
complex analysis that may touch most or all of the 
data.

MapReduce provides a new method of analyzing data that 
is complementary to the capabilities provided by SQL, 
and a system based on MapReduce that can be scaled up 
from single servers to thousands of high and low end 
machines.

*/